import argparse
import re
import sys

import cloudscraper
from bs4 import BeautifulSoup

FP3_URL = 'https://support.fairphone.com/hc/en-us/articles/360048139032-FP3-Fairphone-OS-release-notes'
FP4_URL = 'https://support.fairphone.com/hc/en-us/articles/4405858220945-FP4-Fairphone-OS-Releases-Notes'

DESCRIPTION = \
"""
fpupdatescraper
===============
"""


class Formatter(argparse.ArgumentDefaultsHelpFormatter,
                argparse.RawDescriptionHelpFormatter):
    pass


def fpus_main():
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=Formatter,
    )

    parser.add_argument('-FP3',
                        '--FP3',
                        help="Toggle FP3",
                        action='store_true')
    parser.add_argument('-FP4',
                        '--FP4',
                        help="Toggle FP4",
                        action='store_true')

    args = parser.parse_args()

    if args.FP3:
        url = FP3_URL
    elif args.FP4:
        url = FP4_URL
    else:
        parser.print_help()
        sys.exit()

    scraper = cloudscraper.create_scraper()
    content = scraper.get(url).text
    soup = BeautifulSoup(content, features="html5lib")

    for span in soup.find_all('span',
                              attrs={'class': 'wysiwyg-font-size-large'}):
        updates_info = span.get_text()
        for valid_update_info in re.findall(r'.*[A-Z0-9.]*\d{8}.*',
                                            updates_info):
            version = re.findall(r'[A-Z0-9.-]*\d{8}', valid_update_info)[0]
            android_version = re.findall(r'(Android [\w]*)',
                                         valid_update_info)[0]
            print("Version: " + version)
            print("Android Version: " + android_version)
            print("")


if __name__ == "__main__":
    fpus_main()
